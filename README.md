# realsense-rtabmap-offline

script for recording image and depth data for rtab odometry

## Prerequisite
[https://github.com/IntelRealSense/realsense-ros/wiki/SLAM-with-D435i](realsense-ros slam source)
- realsense2_camera: Follow the installation guide in: https://github.com/intel-ros/realsense.
- imu_filter_madgwick: `sudo apt-get install ros-<distro>-imu-filter-madgwick`
- rtabmap_ros: `sudo apt-get install ros-<distro>-rtabmap-ros`
- robot_localization: `sudo apt-get install ros-<distro>-robot-localization`

