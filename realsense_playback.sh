# if [$# -eq 0] ;
#     then
#         echo 'usage:'
#         echo 'realsense_playback.sh <number>'
#         echo '<number> indicating the index of the bagfile'
#         exit 1
# fi
echo "playback and point cloud file generation in progress ..."
i=$1
dbname="/home/r2c2/realsense/db/realsense_db_"
bagname="/home/r2c2/realsense/bags/realsense_bagfile_"
dbfile="$dbname$i.db"
bagfile="$bagname$i.bag" 
roscore >/dev/null 2>&1 &
rosparam set use_sim_time true
roslaunch realsense2_camera rs_r2.launch >/dev/null 2>&1 &
rosbag play $bagfile >/dev/null 2>&1 &
echo "press any key to stop playback"
read -n 1 -s
echo "Process completed."
rosnode kill --all
sleep 3
cp ~/.ros/rtabmap.db $dbfile
rosnode cleanup
echo "db copied"
exit 0
