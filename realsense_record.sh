#!/bin/bash

rosparam set use_sim_time false
pidmain=$!
echo "roscam pid $pidmain"
# pid[1]=$!
echo ""
echo "Realsense recording..."
echo "Press any key to stop recording."
bagname="/home/r2c2/realsense/bags/realsense_bagfile_"
logname="/home/r2c2/realsense/bags/logs/realsense_logfile_"
dbname="/home/r2c2/realsense/db/realsense_db_"
pcdname
i=0
if [[ -e $bagname$i.bag || -L $bagname$i.bag ]]; then
  while [[ -e $bagname$i.bag || -L $bagname$i.bag ]] ; do
    let i++
  done
  name=$bagname$i
fi
bagfile="$bagname$i.bag"
logfile="$logname$i.log"
dbfile="$dbname$i.db"
touch $logfile

roslaunch realsense2_camera rs_r2.launch >/dev/null 2>&1 &
pidcam=$!
echo "cam pid $pidcam"

rosbag record -O $bagfile \
  /camera/aligned_depth_to_color/camera_info \
  /camera/aligned_depth_to_color/image_raw \
  /camera/aligned_depth_to_color/compressedDepth \
  /camera/color/camera_info \
  /camera/color/image_raw \
  /camera/depth/camera_info \
  /camera/depth/image_rect_raw \
  /imu/data \
  /ImuFilter/parameter_descriptions \
  /ImuFilter/parameter_updates \
  /voxel_cloud \
  /rtabmap/mapData \
  /rtabmap/mapGraph \
  /tf \
  /tf_static \
  __name:=my_bag \
  >/dev/null 2>&1 &

pidbag=$!
echo "bag pid $pidbag"
read -n 1 -s
rosrun pcl_ros pointcloud_to_pcd input:=/map_assembler/cloud_map _prefix:=/home/r2c2/realsense/pcl/${i}_ __name:=pcd_assembler >/dev/null 2>&1 &
pidpcd=$!
echo "exporting point cloud to .pcd file, please wait..."
sleep 2
rosnode kill /pcd_assembler
kill $pidcam
rosnode kill /my_bag
kill $pidbag
echo "Recording has been stored to $bagfile"
echo "Log has been written to $logfile"
echo "Point cloud database copied to $dbfile"
echo "Point cloud exported as pcd to pcddir"
echo ""

: << 'COMMENT'
read -n 1 -s -p "Would you like to perform offline RTAB mapping now? Y/n" mapnow
echo ""
rosparam set use_sim_time true
if [[ $mapnow = "Y" || $mapnow = "y" || $mapnow = "" ]] ; then
  echo "Opening a new terminal for RTAB mapping ..."
  roscore >/dev/null 2>&1 &
  roslaunch realsense2_camera rs_r2.launch offline:=true >/dev/null 2>&1 &
  pidrtab=$!
  rosbag play $bagfile --clock >/dev/null 2>&1 &
  echo "Press any key to stop mapping."
  read -n 1 -s
  kill $pidrtab
  echo "Remember to quit Ros Bag as well."
else
  echo "Process completed."
fi
COMMENT
rosnode kill --all
cp ~/.ros/rtabmap.db $dbfile
rosnode cleanup
exit 1

